![Resize Shape](/Resize%20Shape.png)

# Resize Shape #
Small script for After Effects to resizes Shapes vertices with in/out-tangents to a given value. Should work with deeply nested properties as well.

### Installation: ###
Copy **Resize Shape.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Resize Shape**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------