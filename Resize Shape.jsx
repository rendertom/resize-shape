/**********************************************************************************************
    Resize Shape.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Script resizes Shapes vertices and in/out-tangents to a given value.
    	Should work with deeply nested properties as well.
	
	Todo:
		Add option to toggle "preserve aspect ratio".
**********************************************************************************************/

(function (thisObj) {
	buildUI(thisObj);

	function buildUI(thisObj) {
		var minSpace = 2;
		var txtWidth = 40;
		var fieldWidth = 50;

		var win = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Resize Shape", undefined, {
			resizeable: true
		});
		win.alignChildren = ["fill", "top"];
		win.spacing = minSpace;

		var grpWidth = win.add("group");
		var stWidth = grpWidth.add("statictext", undefined, "Width");
		stWidth.preferredSize.width = txtWidth;
		var etWidth = grpWidth.add("edittext", undefined, "1000");
		etWidth.preferredSize.width = fieldWidth;
		etWidth.alignment = ["fill", "fill"];
		var grpHeight = win.add("group");
		var stHeight = grpHeight.add("statictext", undefined, "Height");
		stHeight.preferredSize.width = txtWidth;
		var etHeight = grpHeight.add("edittext", undefined, "1000");
		etHeight.preferredSize.width = fieldWidth;
		etHeight.alignment = ["fill", "fill"];
		var btnGo = win.add("button", undefined, "Do it!");
		btnGo.onClick = function () {
			var width = parseInt(etWidth.text);
			var height = parseInt(etHeight.text);
			main([width, height]);

		};


		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		if (win instanceof Window) {
			win.center();
			win.show();
		} else {
			win.layout.layout(true);
			win.layout.resize();
		}
	}

	function main(targetSize) {

		var composition = app.project.activeItem;
		if (!composition || !(composition instanceof CompItem))
			return alert("Please select composition first");

		var selectedLayers = composition.selectedLayers;
		if (selectedLayers.length === 0)
			return alert("Please select some Shape Layers");

		var shapeLayers = filterLayersByType(selectedLayers, ShapeLayer);
		if (shapeLayers.length === 0)
			return alert("Please select some Shape Layers");


		app.beginUndoGroup("Scale Shape");

		for (var s = 0, sl = shapeLayers.length; s < sl; s++) {
			scaleShapeLayer(shapeLayers[s], targetSize);
		}

		app.endUndoGroup();
	}

	function scaleShapeLayer(shapeLayer, targetSize) {
		var scaleFactor,
			vectorGroupsArray = [],
			shapePropertiesArray = [],
			vectorPositionProperty;

		scaleFactor = calculateScaleFactor(shapeLayer, targetSize);

		vectorGroupsArray = getProperties(shapeLayer, "ADBE Vector Group");
		if (vectorGroupsArray.length === 0) return;

		for (var v = 0, vl = vectorGroupsArray.length; v < vl; v++) {
			shapePropertiesArray = getProperties(vectorGroupsArray[v], "ADBE Vector Shape");
			if (shapePropertiesArray.length === 0) continue;

			for (var i = 0, il = shapePropertiesArray.length; i < il; i++) {
				scalePath(shapePropertiesArray[i], scaleFactor);
			}

			vectorPositionProperty = vectorGroupsArray[v].property("ADBE Vector Transform Group").property("ADBE Vector Position");
			vectorPositionProperty.setValue(vectorPositionProperty.value * scaleFactor);
		}
		centerLayer(shapeLayer, targetSize);
	}

	function centerLayer(shapeLayer, targetSize) {
		var composition, sourceRect, centerPoint, transformProperty;

		composition = shapeLayer.containingComp;
		sourceRect = shapeLayer.sourceRectAtTime(composition.time, true);
		centerPoint = [
			sourceRect.left + sourceRect.width / 2,
			sourceRect.top + sourceRect.height / 2
		];

		transformProperty = shapeLayer.property("ADBE Transform Group");

		transformProperty.property("ADBE Anchor Point").setValue(centerPoint);
		transformProperty.property("ADBE Position").setValue([composition.width / 2, composition.height / 2]);
		transformProperty.property("ADBE Scale").setValue([100, 100]);
	}

	function scalePath(path, scalar) {
		var pathValue, vertices, inTangents, outTangents, j, jl;

		pathValue = path.value;
		vertices = pathValue.vertices;
		inTangents = pathValue.inTangents;
		outTangents = pathValue.outTangents;

		for (j = 0, jl = vertices.length; j < jl; j++) {
			vertices[j] = vertices[j] * scalar;
			inTangents[j] = inTangents[j] * scalar;
			outTangents[j] = outTangents[j] * scalar;
		}

		pathValue.vertices = vertices;
		pathValue.inTangents = inTangents;
		pathValue.outTangents = outTangents;

		path.setValue(pathValue);
	}

	function getProperties(currentProperty, matchName, propsArray) {
		propsArray = propsArray || [];
		for (var i = 1; i <= currentProperty.numProperties; i++) {
			if (currentProperty.property(i).matchName === matchName) {
				propsArray.push(currentProperty.property(i));
			}
			getProperties(currentProperty.property(i), matchName, propsArray);
		}
		return propsArray;
	}

	function calculateScaleFactor(layer, targetSize) {
		var sourceRect, scaleFactor;

		sourceRect = layer.sourceRectAtTime(layer.containingComp.time, false);
		scaleFactor = [targetSize[0] / sourceRect.width, targetSize[1] / sourceRect.height];

		if (scaleFactor[0] < scaleFactor[1])
			return scaleFactor[0];
		else
			return scaleFactor[1];
	}

	function filterLayersByType(layersArray, instanceObject) {
		var layers = [];
		for (var i = 0, il = layersArray.length; i < il; i++) {
			if (layersArray[i] instanceof instanceObject) {
				layers.push(layersArray[i]);
			}
		}
		return layers;
	}

})(this);